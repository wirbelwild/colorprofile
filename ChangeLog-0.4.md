# Changes in Bit&Black ICC Profile v0.4

## 0.4.1 2021-03-24

### Added

-   Added some new profiles.

## 0.4.0 2021-03-12

### Added

-   Added classes `File` and `FileEnum` to easier load exising profiles.