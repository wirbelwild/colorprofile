<?php

/**
 * Bit&Black Color Profile. Reading ICC Color Profiles.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IccProfile\Test;

use BitAndBlack\IccProfile\File;
use BitAndBlack\IccProfile\FileEnum;
use BitAndBlack\IccProfile\IccProfile;
use PHPUnit\Framework\TestCase;

class IccProfileTest extends TestCase
{
    public function testCanReadFromFile(): void
    {
        $testProfile = new File(
            FileEnum::PROFILE_eciRGB_v2
        );

        $profileFile = new IccProfile($testProfile);

        self::assertSame(
            'RGB',
            $profileFile->getSpace()
        );
    }
    
    public function testCanReadFromStream(): void
    {
        $testProfile = new File(
            FileEnum::PROFILE_eciRGB_v2
        );

        $testStream = file_get_contents($testProfile);

        self::assertIsString($testStream);

        $profileStream = new IccProfile($testStream);

        self::assertSame(
            'RGB',
            $profileStream->getSpace()
        );

        self::assertSame(
            $testStream,
            $profileStream->getContent()
        );
    }
}
