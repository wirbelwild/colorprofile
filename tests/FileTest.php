<?php

/**
 * Bit&Black Color Profile. Reading ICC Color Profiles.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IccProfile\Test;

use BitAndBlack\IccProfile\File;
use BitAndBlack\IccProfile\FileEnum;
use PHPUnit\Framework\TestCase;

/**
 * Class FileTest.
 *
 * @package BitAndBlack\IccProfileTest
 */
class FileTest extends TestCase
{
    public function testCanLoadFile(): void
    {
        $file = (string) new File(
            FileEnum::PROFILE_39L_VDM_U24_754520GM
        );

        self::assertStringContainsString(
            '39L_VDM_U24_754520GM.icc',
            $file
        );
    }
}
