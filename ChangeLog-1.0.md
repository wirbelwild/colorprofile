# Changes in Bit&Black ICC Profile v1.0

## 1.0.0 2024-09-30

### Changed

-   PHP >=8.2 is now required.
-   The namespace has been changed from `BitAndBlack\ICCProfile` into `BitAndBlack\IccProfile`. Please update your implementations.