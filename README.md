[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/iccprofile)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/iccprofile/v/stable)](https://packagist.org/packages/bitandblack/iccprofile)
[![Total Downloads](https://poser.pugx.org/bitandblack/iccprofile/downloads)](https://packagist.org/packages/bitandblack/iccprofile)
[![License](https://poser.pugx.org/bitandblack/iccprofile/license)](https://packagist.org/packages/bitandblack/iccprofile)

<p align="center">
    <a href="https://www.bitandblack.com" target="_blank">
        <img src="https://www.bitandblack.com/build/images/BitAndBlack-Logo-Full.png" alt="Bit&Black Logo" width="400">
    </a>
</p>

# Bit&Black ICC ColorProfile 

Reading ICC Color Profiles. This library is based on the work of Andreas Heigl in [org_heigl/color](https://packagist.org/packages/org_heigl/color).

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/iccprofile). Add it to you project by running `$ composer require bitandblack/iccprofile`.

## Usage 

Set up an `IccProfile` object with the path to a profile or alternatively a string with the profile itself:

````php
<?php

use BitAndBlack\IccProfile\IccProfile;

$iccProfile = new IccProfile('/path/to/SomeProfile.icc');
````

Access the profile's information then:

````php
<?php 

$space = $iccProfile->getSpace();
````

This will get something like `RGB`.

### Using the internal profiles

This library holds a lot of ICC profiles in the `data` folder. To load one of them, you can use the `File` class:

````php
<?php

use BitAndBlack\IccProfile\File;
use BitAndBlack\IccProfile\FileEnum;
use BitAndBlack\IccProfile\IccProfile;

$file = (string) new File(
    FileEnum::PROFILE_39L_VDM_U24_754520GM
);

$iccProfile = new IccProfile($file);
````

## Help 

If you have any questions feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).