<?php

/**
 * Bit&Black Color Profile. Reading ICC Color Profiles.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IccProfile;

use BitAndBlack\IccProfile\Profile\Table\TableInterface;

/**
 * Class NullIccProfile
 *
 * @package BitAndBlack\IccProfile
 */
class NullIccProfile implements IccProfileInterface
{
    /**
     * @return string
     */
    public function getSize(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getCmm(): string
    {
        return '';
    }
    
    /**
     * @return string
     */
    public function getVersion(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getSpace(): string
    {
        return '';
    }
    
    /**
     * @return string
     */
    public function getPcs(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getDatetime(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getSig(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getPlatform(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getFlags(): string
    {
        return '';
    }
    
    /**
     * @return string
     */
    public function getManufacturer(): string
    {
        return '';
    }
    
    /**
     * @return string
     */
    public function getModel(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getAttribs(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getIntent(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getXYZ(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getCreator(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return '';
    }

    /**
     * @return array<string, TableInterface>
     */
    public function getTable(): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return '';
    }
}
