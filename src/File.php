<?php

/**
 * Bit&Black Color Profile. Reading ICC Color Profiles.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IccProfile;

use BitAndBlack\IccProfile\Exception\FileNotFoundException;
use Stringable;

/**
 * The File class is able to handle the path to one of the ICC profiles from the `data` folder.
 *
 * @package BitAndBlack\IccProfile
 */
class File implements Stringable
{
    private readonly string $file;

    /**
     * File constructor.
     *
     * @param FileEnum $fileEnum
     * @throws FileNotFoundException
     */
    public function __construct(FileEnum $fileEnum)
    {
        $folder = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data';
        $file = $folder . DIRECTORY_SEPARATOR . $fileEnum->value;
        
        if (!file_exists($file)) {
            throw new FileNotFoundException($file);
        }
        
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getFile();
    }
    
    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }
}
