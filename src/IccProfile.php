<?php

namespace BitAndBlack\IccProfile;

use BitAndBlack\IccProfile\Exception\FileNotFoundException;
use BitAndBlack\IccProfile\Exception\FileUnreadableException;
use BitAndBlack\IccProfile\Profile\Table\Description;
use BitAndBlack\IccProfile\Profile\Table\TableInterface;
use BitAndBlack\IccProfile\Profile\TableFactory;

class IccProfile implements IccProfileInterface
{
    /**
     * @var string
     */
    private string $size;

    /**
     * @var string
     */
    private string $cmm;

    /**
     * @var string
     */
    private string $version;

    /**
     * @var string
     */
    private string $class;

    /**
     * @var string
     */
    private string $space;

    /**
     * @var string
     */
    private string $pcs;

    /**
     * @var string
     */
    private string $datetime;

    /**
     * @var string
     */
    private string $sig;

    /**
     * @var string
     */
    private string $platform;

    /**
     * @var string
     */
    private string $flags;

    /**
     * @var string
     */
    private string $manufacturer;

    /**
     * @var string
     */
    private string $model;

    /**
     * @var string
     */
    private string $attribs;

    /**
     * @var string
     */
    private string $intent;

    /**
     * @var string
     */
    private string $XYZ;

    /**
     * @var string
     */
    private string $creator;

    /**
     * @var string
     */
    private string $id;

    /**
     * The table of contained profile-information
     *
     * @var array<string, TableInterface> $table
     */
    private array $table = [];

    /**
     * @var string
     */
    private string $content;

    /**
     * IccProfile constructor.
     *
     * @param string $profile
     */
    public function __construct(string $profile)
    {
        $handler = $this->getHandler($profile);

        $this
            ->setSize((string) fread($handler, 4))
            ->setCmm((string) fread($handler, 4))
            ->setVersion((string) fread($handler, 4))
            ->setClass((string) fread($handler, 4))
            ->setSpace(
                trim(
                    (string) fread($handler, 4)
                )
            )
            ->setPcs((string) fread($handler, 4))
            ->setDatetime((string) fread($handler, 12))
            ->setSig((string) fread($handler, 4))
            ->setPlatform((string) fread($handler, 4))
            ->setFlags((string) fread($handler, 4))
            ->setManufacturer((string) fread($handler, 4))
            ->setModel((string) fread($handler, 4))
            ->setAttribs((string) fread($handler, 8))
            ->setIntent((string) fread($handler, 4))
            ->setXYZ((string) fread($handler, 12))
            ->setCreator((string) fread($handler, 4))
            ->setId((string) fread($handler, 16))
        ;
        
        fseek($handler, 128);

        $tagCount = unpack('Nint', (string) fread($handler, 4));

        if (false === $tagCount) {
            return;
        }

        for ($counter = 0; $counter < $tagCount['int']; $counter++) {
            fseek($handler, 128 + 4 + $counter * 12);
            $tagname = (string) fread($handler, 4);
            $offset = unpack('Nint', (string) fread($handler, 4));

            if (false === $offset) {
                return;
            }

            $size = unpack('Nint', (string) fread($handler, 4));

            if (false === $size) {
                return;
            }

            fseek($handler, $offset['int']);
            $tag = TableFactory::getInstance($tagname, (string) fread($handler, $size['int']));
            $this->addTable($tagname, $tag);
        }
    }
    
    /**
     * Add a table to the profile
     *
     * @param string $key
     * @param TableInterface $table
     * @return IccProfile
     */
    private function addTable(string $key, TableInterface $table): self
    {
        if (!$this->hasTable($key)) {
            $this->table[$key] = $table;
        }

        return $this;
    }

    /**
     * Check whether a given Table already exists
     *
     * @param string $key
     * @return boolean
     */
    private function hasTable(string $key): bool
    {
        return isset($this->table[$key]);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $description = $this->table['desc'];

        if (!$description instanceof Description) {
            return '';
        }

        return $description->getContent();
    }
    
    /**
     * @param string $profile
     * @return resource
     * @throws FileNotFoundException
     * @throws FileUnreadableException
     */
    private function getHandler(string $profile)
    {
        $isFile = is_file($profile) && file_exists($profile);
        
        if ($isFile) {
            $handler = fopen($profile, 'rb');
            
            if (!is_resource($handler)) {
                throw new FileUnreadableException($profile);
            }
            
            $this->content = (string) file_get_contents($profile);
            return $handler;
        }

        $handler = @fopen('php://memory', 'rb+');

        if (false !== $handler) {
            $this->content = $profile;
            fwrite($handler, $profile);
            rewind($handler);
            return $handler;
        }
        
        throw new FileNotFoundException($profile);
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @param string $size
     * @return IccProfile
     */
    private function setSize(string $size): self
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return string
     */
    public function getCmm(): string
    {
        return $this->cmm;
    }

    /**
     * @param string $cmm
     * @return IccProfile
     */
    private function setCmm(string $cmm): self
    {
        $this->cmm = $cmm;
        return $this;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     * @return IccProfile
     */
    private function setVersion(string $version): self
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return IccProfile
     */
    private function setClass(string $class): self
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return string
     */
    public function getPcs(): string
    {
        return $this->pcs;
    }

    /**
     * @param string $pcs
     * @return IccProfile
     */
    private function setPcs(string $pcs): self
    {
        $this->pcs = $pcs;
        return $this;
    }

    /**
     * @return string
     */
    public function getDatetime(): string
    {
        return $this->datetime;
    }

    /**
     * @param string $datetime
     * @return IccProfile
     */
    private function setDatetime(string $datetime): self
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * @return string
     */
    public function getSig(): string
    {
        return $this->sig;
    }

    /**
     * @param string $sig
     * @return IccProfile
     */
    private function setSig(string $sig): self
    {
        $this->sig = $sig;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlatform(): string
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     * @return IccProfile
     */
    private function setPlatform(string $platform): self
    {
        $this->platform = $platform;
        return $this;
    }

    /**
     * @return string
     */
    public function getFlags(): string
    {
        return $this->flags;
    }

    /**
     * @param string $flags
     * @return IccProfile
     */
    private function setFlags(string $flags): self
    {
        $this->flags = $flags;
        return $this;
    }

    /**
     * @return string
     */
    public function getManufacturer(): string
    {
        return $this->manufacturer;
    }

    /**
     * @param string $manufacturer
     * @return IccProfile
     */
    private function setManufacturer(string $manufacturer): self
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     * @return IccProfile
     */
    private function setModel(string $model): self
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttribs(): string
    {
        return $this->attribs;
    }

    /**
     * @param string $attribs
     * @return IccProfile
     */
    private function setAttribs(string $attribs): self
    {
        $this->attribs = $attribs;
        return $this;
    }

    /**
     * @return string
     */
    public function getIntent(): string
    {
        return $this->intent;
    }

    /**
     * @param string $intent
     * @return IccProfile
     */
    private function setIntent(string $intent): self
    {
        $this->intent = $intent;
        return $this;
    }

    /**
     * @return string
     */
    public function getXYZ(): string
    {
        return $this->XYZ;
    }

    /**
     * @param string $XYZ
     * @return IccProfile
     */
    private function setXYZ(string $XYZ): self
    {
        $this->XYZ = $XYZ;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreator(): string
    {
        return $this->creator;
    }

    /**
     * @param string $creator
     * @return IccProfile
     */
    private function setCreator(string $creator): self
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return IccProfile
     */
    private function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return array<string, TableInterface>
     */
    public function getTable(): array
    {
        return $this->table;
    }

    /**
     * @return string
     */
    public function getSpace(): string
    {
        return $this->space;
    }

    /**
     * @param string $space
     * @return IccProfile
     */
    private function setSpace(string $space): self
    {
        $this->space = $space;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}
