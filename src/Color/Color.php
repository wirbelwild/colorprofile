<?php

/**
 * Copyright (c)2013-2013 Andreas Heigl, modified by Bit&Black. Original Copyright:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIBILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category  Color
 * @author    Andreas Heigl<andreas@heigl.org>
 * @copyright © 2013-2013 Andreas Heigl
 * @license   http://www.opesource.org/licenses/mit-license.php MIT-License
 * @version   0.0
 * @since     21.03.13
 * @link      https://github.com/heiglandreas/
 */

namespace BitAndBlack\IccProfile\Color;

use Color\Value\CIELab;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\XYZ;

/**
 * Class Color
 *
 * This class represents a PCS-Color which can be defined either as XYZ or L*a*b*
 * Color.
 *
 * @package   Org\Heigl\Color
 * @author    Andreas Heigl<andreas@heigl.org>
 * @copyright © 2013-2013 Andreas Heigl
 * @license   http://www.opesource.org/licenses/mit-license.php MIT-License
 * @version   0.0
 * @since     21.03.13
 * @link      https://github.com/heiglandreas/
 */
class Color
{
    private CIELab $CIELab;

    private XYZ $XYZ;

    /**
     * The Whitepoint for conversion of PCSLab to PCSXYZ and back
     *
     * This conversion whitepoint is defined in Section 6.3.4.3 - Table 14 of
     * Specification ICC.1:2010 (Profile version 4.3.0.0)
     *
     * @var float[] PCSwhitepoint
     */
    private array $PCSwhitepoint = [
        'X' => 96.42,
        'Y' => 100.00,
        'Z' => 82.49,
        'L' => 100.00,
        'a' => 0.0000,
        'b' => 0.0000,
    ];

    /**
     * Color constructor.
     *
     * @param CIELab|XYZ $colorspace
     * @throws InvalidInputNumberException
     */
    public function __construct(CIELab|XYZ $colorspace)
    {
        if ($colorspace instanceof CIELab) {
            $this->setLab($colorspace);
            return;
        }

        $this->setXYZ($colorspace);
    }

    /**
     * @return CIELab
     */
    public function getCIELab(): CIELab
    {
        return $this->CIELab;
    }

    /**
     * @return XYZ
     */
    public function getXYZ(): XYZ
    {
        return $this->XYZ;
    }

    /**
     * Set Lab-values
     *
     * @param CIELab $lab
     * @return Color
     * @throws InvalidInputNumberException
     */
    private function setLab(CIELab $lab): self
    {
        $this->CIELab = $lab;
        $this->setXYZfromLab();
        return $this;
    }

    /**
     * Set XYZ-values from existing Lab-Values
     *
     * @return Color
     * @throws InvalidInputNumberException
     */
    private function setXYZfromLab(): self
    {
        $var_Y = ($this->CIELab->getValue('L') + 16) / 116;
        $var_X = $this->CIELab->getValue('A') / 500 + $var_Y;
        $var_Z = $var_Y - $this->CIELab->getValue('B') / 200;

        if ($var_Y ** 3 > 0.008856) {
            $var_Y = $var_Y ** 3;
        } else {
            $var_Y = ($var_Y - 16 / 116) / 7.787;
        }
        if ($var_X ** 3 > 0.008856) {
            $var_X = $var_X ** 3;
        } else {
            $var_X = ($var_X - 16 / 116) / 7.787;
        }
        if ($var_Z ** 3 > 0.008856) {
            $var_Z = $var_Z ** 3;
        } else {
            $var_Z = ($var_Z - 16 / 116) / 7.787;
        }

        $this->XYZ = new XYZ(
            $this->PCSwhitepoint['X'] * $var_X,
            $this->PCSwhitepoint['Y'] * $var_Y,
            $this->PCSwhitepoint['Z'] * $var_Z
        );

        return $this;
    }

    /**
     * Set the Lab Values from existing XYZ-Values
     *
     * @return Color
     */
    private function setLabFromXYZ(): self
    {
        $this->CIELab = $this->XYZ->getCIELab();
        return $this;
    }

    /**
     * Set the XYZ
     *
     * @param XYZ $xyz
     *
     * @return Color
     */
    private function setXYZ(XYZ $xyz): self
    {
        $this->XYZ = $xyz;
        $this->setLabFromXYZ();
        return $this;
    }
}
