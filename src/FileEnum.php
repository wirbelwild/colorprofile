<?php

/**
 * Bit&Black Color Profile. Reading ICC Color Profiles.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IccProfile;

enum FileEnum: string
{
    case PROFILE_39L_VDM_U24_754520GM = '39L_VDM_U24_754520GM.icc';
    case PROFILE_AdobeRGB1998 = 'AdobeRGB1998.icc';
    case PROFILE_AnimePalette = 'AnimePalette.icc';
    case PROFILE_AppleRGB = 'AppleRGB.icc';
    case PROFILE_BlacklightPoster = 'BlacklightPoster.icc';
    case PROFILE_BlackWhite = 'BlackWhite.icc';
    case PROFILE_CIERGB = 'CIERGB.icc';
    case PROFILE_CoatedFOGRA27 = 'CoatedFOGRA27.icc';
    case PROFILE_CoatedFOGRA39 = 'CoatedFOGRA39.icc';
    case PROFILE_CoatedGRACoL2006 = 'CoatedGRACoL2006.icc';
    case PROFILE_CobaltCarmine = 'CobaltCarmine.icc';
    case PROFILE_ColorMatchRGB = 'ColorMatchRGB.icc';
    case PROFILE_ColorNegative = 'ColorNegative.icc';
    case PROFILE_Dot_Gain_15_prct = 'Dot Gain 15 prct.icc';
    case PROFILE_Dot_Gain_10 = 'DotGain10.icc';
    case PROFILE_Dot_Gain_15 = 'DotGain15.icc';
    case PROFILE_Dot_Gain_20 = 'DotGain20.icc';
    case PROFILE_Dot_Gain_25 = 'DotGain25.icc';
    case PROFILE_Dot_Gain_30 = 'DotGain30.icc';
    case PROFILE_Gamma_1_8 = 'Gamma1_8.icc';
    case PROFILE_Gamma_2_2 = 'Gamma2_2.icc';
    case PROFILE_ECI_RGB_V1_0 = 'ECI-RGB.V1.0.icc';
    case PROFILE_eciRGB_v2 = 'eciRGB_v2.icc';
    case PROFILE_EuropeISOCoatedFOGRA27 = 'EuropeISOCoatedFOGRA27.icc';
    case PROFILE_EuroscaleCoated = 'EuroscaleCoated.icc';
    case PROFILE_EuroscaleUncoated = 'EuroscaleUncoated.icc';
    case PROFILE_GoldBlue = 'GoldBlue.icc';
    case PROFILE_GoldCrimson = 'GoldCrimson.icc';
    case PROFILE_GreenRed = 'GreenRed.icc';
    case PROFILE_HDR_P3_D65_ST2084 = 'HDR_P3_D65_ST2084.icc';
    case PROFILE_image_P3 = 'image-P3.icc';
    case PROFILE_ISOcoated_v2_300_eci = 'ISOcoated_v2_300_eci.icc';
    case PROFILE_ISOcoated_v2_eci = 'ISOcoated_v2_eci.icc';
    case PROFILE_ISOuncoatedyellowish = 'ISOuncoatedyellowish.icc';
    case PROFILE_JapanColor2001Coated = 'JapanColor2001Coated.icc';
    case PROFILE_JapanColor2001Uncoated = 'JapanColor2001Uncoated.icc';
    case PROFILE_JapanColor2002Newspaper = 'JapanColor2002Newspaper.icc';
    case PROFILE_JapanColor2003WebCoated = 'JapanColor2003WebCoated.icc';
    case PROFILE_JapanStandard = 'JapanStandard.icc';
    case PROFILE_JapanWebCoated = 'JapanWebCoated.icc';
    case PROFILE_P3D65 = 'P3D65.icc';
    case PROFILE_PAL_SECAM = 'PAL_SECAM.icc';
    case PROFILE_Pastel8Hues = 'Pastel8Hues.icc';
    case PROFILE_ProPhoto = 'ProPhoto.icm';
    case PROFILE_PSO_Coated_300_NPscreen_ISO12647_eci = 'PSO_Coated_300_NPscreen_ISO12647_eci.icc';
    case PROFILE_PSO_Coated_NPscreen_ISO12647_eci = 'PSO_Coated_NPscreen_ISO12647_eci.icc';
    case PROFILE_PSO_LWC_Improved_eci = 'PSO_LWC_Improved_eci.icc';
    case PROFILE_PSO_LWC_Standard_eci = 'PSO_LWC_Standard_eci.icc';
    case PROFILE_PSO_MFC_Paper_eci = 'PSO_MFC_Paper_eci.icc';
    case PROFILE_PSO_SNP_Paper_eci = 'PSO_SNP_Paper_eci.icc';
    case PROFILE_PSO_Uncoated_ISO12647_eci = 'PSO_Uncoated_ISO12647_eci.icc';
    case PROFILE_PSO_Uncoated_NPscreen_ISO12647_eci = 'PSO_Uncoated_NPscreen_ISO12647_eci.icc';
    case PROFILE_PSOcoated_v3 = 'PSOcoated_v3.icc';
    case PROFILE_PSOuncoated_v3_FOGRA52 = 'PSOuncoated_v3_FOGRA52.icc';
    case PROFILE_PSR_LWC_PLUS_V2_PT = 'PSR_LWC_PLUS_V2_PT.icc';
    case PROFILE_PSR_LWC_STD_V2_PT = 'PSR_LWC_STD_V2_PT.icc';
    case PROFILE_PSR_SC_PLUS_V2_PT = 'PSR_SC_PLUS_V2_PT.icc';
    case PROFILE_PSR_SC_STD_V2_PT = 'PSR_SC_STD_V2_PT.icc';
    case PROFILE_PSRgravureMF = 'PSRgravureMF.icc';
    case PROFILE_QuarkGenericCMYK = 'QuarkGenericCMYK.icc';
    case PROFILE_QuarkGenericGray = 'QuarkGenericGray.icc';
    case PROFILE_QuarkGenericLab = 'QuarkGenericLab.icc';
    case PROFILE_QuarkGenericRGB = 'QuarkGenericRGB.icc';
    case PROFILE_QuarkXPressEmulateGray = 'QuarkXPressEmulateGray.icc';
    case PROFILE_QuarkXPressLegacyCMYK = 'QuarkXPressLegacyCMYK.icc';
    case PROFILE_QuarkXPressLegacyRGB = 'QuarkXPressLegacyRGB.icc';
    case PROFILE_RedBlueYelllow = 'RedBlueYelllow.icc';
    case PROFILE_SC_paper_eci = 'SC_paper_eci.icc';
    case PROFILE_SiennaBlue = 'SiennaBlue.icc';
    case PROFILE_Smokey = 'Smokey.icc';
    case PROFILE_SMPTE_C = 'SMPTE-C.icc';
    case PROFILE_sRGB_Color_Space_Profile = 'sRGB Color Space Profile.icm';
    case PROFILE_sRGB_IEC61966_2_1 = 'sRGB IEC61966-2.1.icc';
    case PROFILE_TealMagentaGold = 'TealMagentaGold.icc';
    case PROFILE_TotalInkPreview = 'TotalInkPreview.icc';
    case PROFILE_TurquoiseSepia = 'TurquoiseSepia.icc';
    case PROFILE_UncoatedFOGRA29 = 'UncoatedFOGRA29.icc';
    case PROFILE_USNewsprintSNAP2007 = 'USNewsprintSNAP2007.icc';
    case PROFILE_USSheetfedCoated = 'USSheetfedCoated.icc';
    case PROFILE_USSheetfedUncoated = 'USSheetfedUncoated.icc';
    case PROFILE_USWebCoatedSWOP = 'USWebCoatedSWOP.icc';
    case PROFILE_USWebUncoated = 'USWebUncoated.icc';
    case PROFILE_WebCoatedFOGRA28 = 'WebCoatedFOGRA28.icc';
    case PROFILE_WebCoatedSWOP2006Grade3 = 'WebCoatedSWOP2006Grade3.icc';
    case PROFILE_WebCoatedSWOP2006Grade5 = 'WebCoatedSWOP2006Grade5.icc';
    case PROFILE_WideGamutRGB = 'WideGamutRGB.icc';
}
