<?php

/**
 * Bit&Black Color Profile. Reading ICC Color Profiles.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IccProfile;

use BitAndBlack\IccProfile\Profile\Table\TableInterface;

/**
 * Interface IccProfileInterface
 *
 * @package BitAndBlack\IccProfile
 */
interface IccProfileInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getSize(): string;

    /**
     * @return string
     */
    public function getCmm(): string;
   
    /**
     * @return string
     */
    public function getVersion(): string;

    /**
     * @return string
     */
    public function getClass(): string;

    /**
     * @return string
     */
    public function getSpace(): string;

    /**
     * @return string
     */
    public function getPcs(): string;

    /**
     * @return string
     */
    public function getDatetime(): string;
    
    /**
     * @return string
     */
    public function getSig(): string;
    
    /**
     * @return string
     */
    public function getPlatform(): string;
   
    /**
     * @return string
     */
    public function getFlags(): string;

    /**
     * @return string
     */
    public function getManufacturer(): string;

    /**
     * @return string
     */
    public function getModel(): string;
   
    /**
     * @return string
     */
    public function getAttribs(): string;

    /**
     * @return string
     */
    public function getIntent(): string;

    /**
     * @return string
     */
    public function getXYZ(): string;
  
    /**
     * @return string
     */
    public function getCreator(): string;
   
    /**
     * @return string
     */
    public function getId(): string;
   
    /**
     * @return array<string, TableInterface>
     */
    public function getTable(): array;

    /**
     * @return string
     */
    public function getContent(): string;
}
