<?php

/**
 * Copyright (c)2013-2013 Andreas Heigl, modified by Bit&Black. Original Copyright:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIBILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category
 * @author    Andreas Heigl<andreas@heigl.org>
 * @copyright ©2013-2013 Andreas Heigl
 * @license   http://www.opesource.org/licenses/mit-license.php MIT-License
 * @version   0.0
 * @since     18.04.13
 * @link      https://github.com/heiglandreas/
 */

namespace BitAndBlack\IccProfile\Profile\Table;

class ClutEntry implements ClutInterface
{
    /**
     * @var array<int, ClutInterface>
     */
    protected array $entry = [];

    /**
     * @param ClutInterface $entry
     * @return ClutEntry
     * @see ClutInterface::addEntry
     */
    public function addEntry(ClutInterface $entry): self
    {
        $this->entry[] = $entry;
        return $this;
    }

    /**
     * Create an instance of the class
     *
     * @param array<int, ClutInterface> $content
     */
    public function __construct(array|null $content = null)
    {
        if (null === $content) {
            return;
        }

        $this->entry = $content;
    }

    /**
     * Get the value for this entry
     *
     * @param float[] $input
     * @return array<int, ClutInterface>
     * @todo Unclear, why we have a parameter here.
     */
    public function getValue(array $input): array
    {
        return $this->entry;
    }
}
