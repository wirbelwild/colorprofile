<?php

/**
 * Copyright (c)2013-2013 Andreas Heigl, modified by Bit&Black. Original Copyright:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIBILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category
 * @author    Andreas Heigl<andreas@heigl.org>
 * @copyright ©2013-2013 Andreas Heigl
 * @license   http://www.opesource.org/licenses/mit-license.php MIT-License
 * @version   0.0
 * @since     18.04.13
 * @link      https://github.com/heiglandreas/
 */

namespace BitAndBlack\IccProfile\Profile\Table;

use BitAndBlack\IccProfile\Exception as Ex;

class Clut implements ClutInterface
{
    /**
     * @phpstan-ignore-next-line
     */
    protected array $entry = [];

    /**
     * @phpstan-ignore-next-line
     */
    private int $outputChannels;

    /**
     * @phpstan-ignore-next-line
     */
    private int $inputChannels;

    /**
     * @phpstan-ignore-next-line
     */
    private int $clutPoints;

    /**
     * Add an entry to the ColorLookupTable
     *
     * @param ClutInterface $entry
     * @return Clut
     * @see ClutInterface::addEntry()
     */
    public function addEntry(ClutInterface $entry): self
    {
        $this->entry[] = $entry;
        return $this;
    }

    /**
     * Create an instance of the class
     *
     * @param ClutInterface[] $content
     */
    public function __construct(array|null $content = null)
    {
        if (null === $content) {
            return;
        }

        foreach ($content as $item) {
            if (!$item instanceof ClutInterface) {
                continue;
            }

            $this->addEntry($item);
        }
    }

    /**
     * set the number of input channels
     *
     * @param int $inputChannels
     * @return Clut
     */
    public function setInputChannels(int $inputChannels): self
    {
        $this->inputChannels = $inputChannels;
        return $this;
    }

    /**
     * Set the number of outputChannels
     *
     * @param int $outputChannels
     * @return Clut
     */
    public function setOutputChannels(int $outputChannels): self
    {
        $this->outputChannels = $outputChannels;
        return $this;
    }

    /**
     * Set the number of CLUT-Points
     *
     * @param int $clutPoints
     * @return Clut
     */
    public function setClutPoints(int $clutPoints): self
    {
        $this->clutPoints = $clutPoints;
        return $this;
    }

    /**
     * Get the number of CLUT-Points
     *
     * @return int
     */
    public function getClutPoints(): int
    {
        return count($this->entry);
    }

    /**
     * Get a value
     *
     * The input value has to be given as float between 0 and 1 including.
     *
     * This method takes the input, multiplies it with the number of CLUT-Points
     * and returns the linearized result between the ceiling- and floor-values
     *
     * @param float[] $input
     * @throws Ex\SizeMismatchException
     * @return array<int, int|float>
     */
    public function getValue(array $input): array
    {
        $value = array_shift($input);

        $value *= ($this->getClutPoints() - 1);

        $valueFloor = floor($value);
        $valueCeil = ceil($value);

        $floor = $this->entry[$valueFloor]->getValue($input);
        $ceil = $this->entry[$valueCeil]->getValue($input);

        if ((is_countable($floor) ? count($floor) : 0) != (is_countable($ceil) ? count($ceil) : 0)) {
            throw new Ex\SizeMismatchException('The two arrays are not of the same size');
        }

        $returnValue = [];
        $count = is_countable($floor) ? count($floor) : 0;
        $diff = $value - $valueFloor;

        for ($i = 0; $i < $count; $i++) {
            $myVal = ($ceil[$i] - $floor[$i]) * $diff;
            $returnValue[$i] = $floor[$i] + $myVal;
        }

        return $returnValue;
    }
}
