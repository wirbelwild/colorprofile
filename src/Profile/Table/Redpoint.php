<?php

/**
 * Copyright (c)2013-2013 Andreas Heigl, modified by Bit&Black. Original Copyright:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIBILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category
 * @author    Andreas Heigl<andreas@heigl.org>
 * @copyright ©2013-2013 Andreas Heigl
 * @license   http://www.opesource.org/licenses/mit-license.php MIT-License
 * @version   0.0
 * @since     26.03.13
 * @link      https://github.com/heiglandreas/
 */

namespace BitAndBlack\IccProfile\Profile\Table;

use BitAndBlack\IccProfile\Color\Color;
use BitAndBlack\IccProfile\Parser\S15Fixed16Number;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\XYZ;

class Redpoint implements TableInterface
{
    /**
     * Color of the red-Max-Point
     *
     * @var Color|null
     */
    protected ?Color $redpoint = null;

    /**
     * Set the RedPoint
     *
     * @param Color $redpoint
     *
     * @return Redpoint
     */
    public function setRedpoint(Color $redpoint): Redpoint
    {
        $this->redpoint = $redpoint;
        return $this;
    }

    /**
     * Get the redpoint
     *
     * @return Color|null
     */
    public function getRedpoint(): ?Color
    {
        return $this->redpoint;
    }

    /**
     * Set the content of this class
     *
     * @param string $content
     * @return Redpoint
     * @throws InvalidInputNumberException
     */
    public function setContent(string $content): Redpoint
    {
        $x = substr($content, 8, 4);
        $y = substr($content, 12, 4);
        $z = substr($content, 16, 4);
        $x = S15Fixed16Number::toFloat($x);
        $y = S15Fixed16Number::toFloat($y);
        $z = S15Fixed16Number::toFloat($z);

        $color = new Color(
            new XYZ($x, $y, $z)
        );

        $this->setRedpoint($color);

        return $this;
    }

    /**
     * Create an instance of the class
     *
     * @param Color|null $redpoint
     */
    public function __construct(Color|null $redpoint = null)
    {
        if (null !== $redpoint) {
            $this->setRedpoint($redpoint);
        }
    }
}
