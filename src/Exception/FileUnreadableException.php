<?php

/**
 * Bit&Black Color Profile. Reading ICC Color Profiles.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IccProfile\Exception;

use InvalidArgumentException;

/**
 * Class FileUnreadableException
 *
 * @package BitAndBlack\IccProfile\Exception
 */
class FileUnreadableException extends InvalidArgumentException
{
    /**
     * FileUnreadableException constructor.
     *
     * @param string $file
     */
    public function __construct(string $file)
    {
        parent::__construct('The file ' . $file . ' is unreadable');
    }
}
