<?php

/**
 * Bit&Black Color Profile. Reading ICC Color Profiles.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IccProfile\Exception;

use InvalidArgumentException;

/**
 * Class FileNotFoundException
 *
 * @package BitAndBlack\IccProfile\Exception
 */
class FileNotFoundException extends InvalidArgumentException
{
    /**
     * FileNotFoundException constructor.
     *
     * @param string $file
     */
    public function __construct(string $file)
    {
        parent::__construct('The file ' . $file . ' can\'t be found');
    }
}
